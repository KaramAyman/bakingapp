package com.example.karam.bakingapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.karam.bakingapp.DataModels.BackingObj;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class BakingActivity extends AppCompatActivity implements AddapterView.AddapterOnClickListener {
    private static final String url_For_json =
            "https://d17h27t6h515a5.cloudfront.net/topher/2017/May/59121517_baking/baking.json";
    private TextView mErrorMessage;
    private ProgressBar mProgressBar;
    public RecyclerView mRecyclerView;
    private AddapterView mAddapter;
    public Context context;
    List<BackingObj> mBakingJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baking);
        context = BakingActivity.this;


        mErrorMessage = (TextView) findViewById(R.id.textView_error_message_display);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_loading_indicator);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager linearVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);

        mAddapter = new AddapterView(this, this);
        mRecyclerView.setAdapter(mAddapter);
        loadBakingData();
    }

    public void loadBakingData() {
        showBakingData();
        new FetchMovieTask().execute(url_For_json);
    }

    public void showBakingData() {
        mErrorMessage.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    public void showErrorMessage() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        mErrorMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void OnClick(BackingObj mClickedName) {
        Intent intent = new Intent(BakingActivity.this, DetailsOfBakingActivity.class);
        Bundle mBundle = new Bundle();

        mBundle.putParcelable("backingObj", mClickedName);
        intent.putExtras(mBundle);
        startActivity(intent);


    }

    private class FetchMovieTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length == 0) {
                return null;
            }
            String url = strings[0];
            URL BakingUrl = NetworkUtiles.BuildUrl(url);
            String jsonBakingResponse = null;
            try {
                jsonBakingResponse = NetworkUtiles.getResponseFromHttpUrl(BakingUrl);
                return jsonBakingResponse;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            mProgressBar.setVisibility(View.INVISIBLE);
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<BackingObj>>() {
            }.getType();

            try {
                mBakingJson = gson.fromJson(s, type);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (mBakingJson != null) {
                mAddapter.setBakingData(mBakingJson);
                showBakingData();
            } else {
                showErrorMessage();
            }
        }
    }
}
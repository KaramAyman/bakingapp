package com.example.karam.bakingapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.karam.bakingapp.DataModels.BackingObj;
import java.util.ArrayList;
import java.util.List;

public class AddapterView extends RecyclerView.Adapter<AddapterView.ForecastAdapterViewHolder> {
    private List<BackingObj> mBakingJson;
    Context mContext;
    private final AddapterOnClickListener mClickListener;
    public interface AddapterOnClickListener{
        void OnClick(BackingObj mClickedName);
    }
    public AddapterView(Context context,AddapterOnClickListener clickListener) {
        mBakingJson = new ArrayList<>();
        mContext = context;
        mClickListener = clickListener;
    }
    public class ForecastAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView bakingItemTV;

        public ForecastAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            bakingItemTV = (TextView) itemView.findViewById(R.id.name_of_baking);
           itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            int addapterPosition =getAdapterPosition();
            BackingObj ClickedName= mBakingJson.get(addapterPosition);
            mClickListener.OnClick(ClickedName);
        }
    }
    public void setBakingData(List<BackingObj> bakingList) {
        mBakingJson = bakingList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ForecastAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int layoutIdForBakingItem = R.layout.baking_item;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(layoutIdForBakingItem, viewGroup, false);
        return new ForecastAdapterViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ForecastAdapterViewHolder forecastAdapterViewHolder, int position) {
        // You Use This To Full The RecyclerView Items
        String nameOfBaking = mBakingJson.get(position).getName();
        forecastAdapterViewHolder.bakingItemTV.setText(nameOfBaking);
    }

    @Override
    public int getItemCount(){return mBakingJson.size();}
}
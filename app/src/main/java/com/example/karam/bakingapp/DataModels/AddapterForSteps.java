package com.example.karam.bakingapp.DataModels;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karam.bakingapp.R;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

public class AddapterForSteps extends RecyclerView.Adapter<AddapterForSteps.ForecastAdapterViewHolder> {
    Context mContext;

    private List<Step> msteps;

    public AddapterForSteps(Context context) {
        msteps = new ArrayList<>();
        mContext = context;
    }


    @NonNull
    @Override
    public ForecastAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int layoutForOneStep = R.layout.steps_items;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(layoutForOneStep, viewGroup, false);
        return new ForecastAdapterViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ForecastAdapterViewHolder forecastAdapterViewHolder, int position) {

        String description = msteps.get(position).getDescription();
        forecastAdapterViewHolder.stepsTV.setText(description);

        // we get the url from msteps and play the vedio
        if (msteps.get(position).getVideoURL() != null) {
            Uri mediauri = Uri.parse(msteps.get(position).getVideoURL());
            TrackSelector trackSelector = new DefaultTrackSelector();
            LoadControl loadControl = new DefaultLoadControl();
            forecastAdapterViewHolder.mExoplayer = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector, loadControl);
            forecastAdapterViewHolder.mPlayerView.setPlayer(forecastAdapterViewHolder.mExoplayer);
            String userAgent = Util.getUserAgent(mContext, "BakingApp");
            MediaSource mediaSource = new ExtractorMediaSource(mediauri, new DefaultDataSourceFactory(
                    mContext, userAgent), new DefaultExtractorsFactory(), null, null);
            forecastAdapterViewHolder.mExoplayer.prepare(mediaSource);
            forecastAdapterViewHolder.mExoplayer.setPlayWhenReady(true);


        }

    }

    @Override
    public int getItemCount() {
        return msteps.size();
    }

    public class ForecastAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView stepsTV;
        public SimpleExoPlayer mExoplayer;
        private SimpleExoPlayerView mPlayerView;


        public ForecastAdapterViewHolder(@NonNull View itemView) {

            super(itemView);
            mPlayerView = (SimpleExoPlayerView) itemView.findViewById(R.id.playerView);
            stepsTV = (TextView) itemView.findViewById(R.id.stepsTV);
        }
    }

    public void setStepsData(List<Step> steps) {
        msteps = steps;
        notifyDataSetChanged();
    }


}

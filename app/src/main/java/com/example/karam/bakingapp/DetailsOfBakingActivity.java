package com.example.karam.bakingapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.karam.bakingapp.DataModels.AddapterForSteps;
import com.example.karam.bakingapp.DataModels.BackingObj;
import com.example.karam.bakingapp.DataModels.Ingredient;
import com.example.karam.bakingapp.DataModels.Step;

import java.util.List;

public class DetailsOfBakingActivity extends AppCompatActivity {

    public RecyclerView mRecyclerView;

    Context context;
    private Bundle reternedBakingObj;
    public TextView ingredientTV;
    public TextView presentationOfingredientTV;
    public AddapterForSteps maddapterForSteps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_of_baking);
        context= DetailsOfBakingActivity.this;
        maddapterForSteps=new AddapterForSteps(context);
        presentationOfingredientTV=(TextView) findViewById(R.id.textForPresentingIngrdient);
        ingredientTV = (TextView) findViewById(R.id.textViewForIngredient);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerviewForDetailsActivity);
        LinearLayoutManager linearVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearVertical);


        mRecyclerView.setAdapter(maddapterForSteps);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        BackingObj mbackingObj = getIntent().getExtras().getParcelable("backingObj");

        List<Ingredient> mingredient = mbackingObj.getIngredients();
        String Ingredients = "";

        for (int i =0 ; i<mingredient.size();i++) {
            Ingredients = Ingredients.
                    concat(String.valueOf(mingredient.get(i).getQuantity())+" ")
                    .concat(mingredient.get(i).getMeasure())
                    .concat(" of ").concat(mingredient.get(i).getIngredient()).concat("\n");
        }
        presentationOfingredientTV.setText("The Ingredients For This Baking Are :");
        ingredientTV.setText(Ingredients);

        List<Step> mSteps = mbackingObj.getSteps();

        maddapterForSteps.setStepsData(mSteps);






    }
}
